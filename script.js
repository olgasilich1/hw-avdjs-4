// AJAX(Asynchronous Javascript and XML) - это набор методов вебразработки,
//которые позволяют веб-приложениям работать асинхронно тоесть обрабатывать
// запросы к серверу в фотовом режиме
// Полезность AJAX заключается в том, что он позволяет обращятся к серверу
// без перезагрузки

let div = document.querySelector('#listDiv');

function createCharactersList(div) {
  let ul = document.createElement('ul');
  fetch('https://swapi.dev/api/films/')
    .then((response) => response.json())
    .then((data) => {
      data.results.forEach((episode) => {
        let liEpisode = document.createElement('li');
        liEpisode.innerHTML = `${episode.title}; episode number: ${episode.episode_id}, description: ${episode.opening_crawl}`;
        episode.characters.forEach((character) => {
          const ulCharacters = document.createElement('ul');
          fetch(character)
            .then((response) => response.json())
            .then((data) => {
              const liCharacter = document.createElement('li');
              liCharacter.innerHTML = data.name;
              ulCharacters.appendChild(liCharacter);
            });
          liEpisode.appendChild(ulCharacters);
        });
        ul.appendChild(liEpisode);
      });
      console.log(data.results);
    });
  div.appendChild(ul);
}

createCharactersList(div);
